#!/bin/bash

user_choice=$1
arr[0]="rock"
arr[1]="paper"
arr[2]="scissors"
arr_size=${#arr[@]}
index=$(($RANDOM % $arr_size))
computer_choice=${arr[$index]}

function check_result(){
	if [[ $user_choice == $computer_choice ]]; then
		echo "Tie $user_choice $computer_choice"
	elif [[ $user_choice == "${arr[0]}" && $computer_choice == "${arr[2]}" ]]; then
	       echo "You WIN! $user_choice $computer_choice"
        elif [[ $user_choice == "${arr[1]}" && $computer_choice == "${arr[0]}" ]]; then
 		echo "You WIN! $user_choice $computer_choice"		       
	elif [[ $user_choice == "${arr[2]}" && $computer_choice == "${arr[1]}" ]]; then
		echo "You WIN! $user_choice $computer_choice"
	elif [[ ($user_choice == "${arr[0]}" && $computer_choice == "${arr[1]}") || ($user_choice == "${arr[1]}" && $computer_choice == "${arr[2]}") || ($user_choice == "${arr[2]}" && $computer_choice == "${arr[0]}") ]]; then
		echo "You lose! $user_choice $computer_choice"
	else
		echo "something went wrong $user_choice $computer_choice"
	fi
}

check_result
