#!/bin/bash

for (( i=1; i <= 30; i++ ))
do
	if (( $i % 5 == 0  && $i % 3 == 0 )); then
		echo "$i FizzBuzz"
	elif (( $i % 3 == 0 )); then 
		echo "$i Fizz" 
	elif (( $i % 5 == 0 )); then
		echo "$i Buzz"
	fi
done
