#!/bin/bash

stop_focus=`date -d '+20 min'  | awk '{print $5}'`
while :
do
	start_focus=`date | awk '{print $5}'`
	echo "Start focus:$start_focus Stop focus:$stop_focus"
	if [[ "${start_focus}" < "${stop_focus}" ]]; then
		sleep 1
	else
		echo -ne '\007'
		echo -e "\e[31m end of focus \e[0m"
		exit
	fi	
done
