#!/bin/bash

function check_result(){
	if (( $user_choice == $digit_to_find )); then
		echo "You WIN! Attempt $counter"
		exit
	else
		check_number
	fi
}

function check_number(){
        first_digit="${user_choice:0:1}"
	second_digit="${user_choice:1:1}"
	third_digit="${user_choice:2:1}"	
	echo "$first_digit $second_digit $third_digit"
	if (( $first_digit == "${digit_to_find:0:1}" || $second_digit == "${digit_to_find:1:1}" || $third_digit == "${digit_to_find:2:1}" )); then
		echo "fermi $first_digit $second_digit $third_digit"
	elif (( $first_digit == "${digit_to_find:1:1}" || $first_digit == "${digit_to_find:2:1}" || $second_digit == "${digit_to_find:0:1}" || $second_digit == "${digit_to_find:2:1}" || $third_digit == "${digit_to_find:0:1}" || $third_digit == "${digit_to_find:1:1}" )); then
		echo "piko $first_digit $second_digit $third_digit"
	else
		echo "bajgle $first_digit $second_digit $third_digit"
	fi	
}

digit_to_find=123

for (( counter=1; counter<=10; counter++ )); do
	echo "Enter your value"
	read user_choice
	echo "Your attempt: $counter "
	check_result
done
	
