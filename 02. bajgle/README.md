./run.sh

You have 10 tries to guess a 3-digit number

hints:
Pico
One digit is correct, but it is in the wrong position.
Fermi
One digit is correct and is in the correct position.
Bagels
No digit is correct.
