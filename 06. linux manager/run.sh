#!/bin/bash

# Volume Settings
function volumeSettings(){
	while :
	do
		echo "Volume settings:"
		echo "1. Current volume level"
		echo "2. Volume Mute"
		echo "3. Volume UP"
		echo "4. Volume DOWN"
		echo "5. BACK"
		echo " > > "
		read user_choose
		case $user_choose in
			"1") volumeCurrentLevel ;;
			"2") volumeMute ;;
			"3") volumeUp ;;
			"4") volumeDown ;;
			"5") mainMenu ;;
		esac
	done
}

function volumeCurrentLevel(){
	echo "Current volume level: "
	awk -F"[][]" '/dB/ { print $2 }' <(amixer sget Master)
}

function volumeMute(){
	amixer set 'Master' 0%
}

function volumeDown(){
	amixer set 'Master' 10%-
}

function volumeUp(){
	amixer set 'Master' 10%+	
}

# Device Summary
function deviceSummary(){
	echo "Uptime: `uptime | awk '{print $1}'`"
	echo ''
	echo "Current user: `whoami`"
	echo ''
	echo "Processor: `lscpu | grep 'Model name:'`"
	echo ''
	echo "Disk: `ls /dev/disk/by-id | awk '{print $NF}' | grep -v '[part1|part2]$'` "
	df --output=used,avail -h *
	echo ''
	echo "Memory: "
	cat /proc/meminfo | grep 'MemTotal'
	cat /proc/meminfo | grep 'MemFree'
	cat /proc/meminfo | grep 'MemAvailable'
	echo ''
}


# Menu Program
function mainMenu(){
	echo "Menu: "
	echo "1. Volume settings"
	echo "9. Device summary"
	echo "0. Exit program"
}

function programExit(){
	exit
}


# Main
while : do
	mainMenu
	read user_choose
	case $user_choose in
		"1") volumeSettings ;;
		"9") deviceSummary ;; 
		"0") programExit ;;
	esac
done
