#!/bin/bash


function checkProgram(){
	echo "enter the application name"
	read application_name
	apt list | grep $application_name	
}

function installProgram(){
	echo "enter the application name"
	read application_name
	sudo apt-get update && sudo apt-get install $application_name
}

function uninstallProgram(){
	echo "enter the application name"
	read application_name
	sudo apt-get remove $application_name
}

function exitProgram(){
	echo 'exit' 
	exit
}

function menuProgram(){
	echo "Menu:"
	echo "1. Check installed programs"
	echo "2. Install program"
	echo "3. Uninstall program"
	echo "0. Exit"
}

#Main

while :
do
	menuProgram
	echo "Choose option: "
	read user_choice
	case $user_choice in
		"1") checkProgram ;;
		"2") installProgram ;;
		"3") uninstallProgram ;;
		"0") exitProgram ;;
		*) echo "wrong option"
	esac
done
